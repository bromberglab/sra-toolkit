#!/bin/bash

#
# evenfasterq-dump is an alternative way to download SRR|ERR|DRR read files
# directly from the NCBI SRA archive for situations where fasterq-dump
# (or the older fastq-dump) are either too slow or simply fail.
#
# Parts of the code was based on wonderdump (http://data.biostarhandbook.com/scripts/wonderdump.sh)
#
# Usage:
#   evenfasterq-dump <fasterq-dump options> <SRR|ERR|DRR>
#
# Example:
# evenfasterq-dump SRR000001
# 

set -u

VERSION=1.2

if [ -z ${SRA_TOOLKIT_BASE+x} ]
then
  SRA_TOOLKIT_BASE=/usr/local
fi

if [ -z ${ASPERA_BASE+x} ]
then
  ASPERA_BASE=/usr/local/share/aspera
fi

if [ -z ${FASTQ_FALLBACK+x} ]
then
  DUMP_TOOL="FAST-ER-Q_DUMP"
  FASTQDUMP_BIN=${SRA_TOOLKIT_BASE}/bin/fasterq-dump
else
  DUMP_TOOL="FASTQ_DUMP"
  FASTQDUMP_BIN=${SRA_TOOLKIT_BASE}/bin/fastq-dump
fi

ASPERA_BIN=${ASPERA_BASE}/connect/bin/ascp
ASPERA_KEY=${ASPERA_BASE}/connect/etc/asperaweb_id_dsa.openssh
ASPERA_PARAMS="-i ${ASPERA_KEY} -k1 -T -l100m"
PREFETCH_BIN=${SRA_TOOLKIT_BASE}/bin/prefetch
PREFETCH_PARAMS="-q -a \"${ASPERA_BIN}|${ASPERA_KEY}\" --max-size 100000000"

# All other parameters before last.
# PARAMS=${@:1:$(($# - 1))}

# The last parameter must be the SRR number.
# SRA_ID=${@:$#}
usage="$(basename "$0") (SRR|ERR|DRR)_ID[ 2nd_ID 3rd_ID ... ] [--sralist PATH_TO_SRALIST_FILE] [-O|--output-directory|--outdir PATH_TO_DIR] [-o|--output-file|--outfile OUT_FILE_NAME] [-h|--help] [-v|--version] -- download SRR|ERR|DRR read files (in batch) from the NCBI SRA archive"

POSITIONAL=()
while [[ $# -gt 0 ]]; do
 case "$1" in
  -O|--output-directory|--outdir)
    shift
    if [[ "$1" = /* ]]
    then
      OUT_DIR=$1
    else
      OUT_DIR=${OUTPUT_PATH}/$1
    fi
    shift
    ;;
  -o|--output-file|--outfile)
    shift
    OUT_FILE=$1
    shift
    ;;
  --sralist)
    shift
    if [[ "$1" = /* ]]
    then
      SRA_LIST=$1
    else
      SRA_LIST=${INPUT_PATH}/$1
    fi
    shift
    ;;
  -h|--help)
    echo "$usage"
    exit
    ;;
  -v|--version)
    printf "even-fasterq: ${VERSION}\n`fasterq-dump --version`\n`/usr/local/share/aspera/connect/bin/ascp --version`" 
    exit
    ;;
  *)
    POSITIONAL+=("$1")
    shift
    ;;
  esac
done

if [ -z ${OUT_DIR+x} ]
then
  OUT_DIR=${OUTPUT_PATH}
fi

if [ -z ${SRA_TMP_DIR+x} ]
then
  TMP_DIR=${OUT_DIR}/tmp
else
  TMP_DIR=${SRA_TMP_DIR}/sra
fi

logfile="${OUT_DIR}/evenfasterq-dump.log"
mkdir -p "${OUT_DIR}"
mkdir -p "${TMP_DIR}"

if [ ! -z ${SRA_LIST+x} ];
then
    if [ -z ${POSITIONAL+x} ];
    then
        POSITIONAL+=("")
    fi
    set -- "${POSITIONAL[@]}"
    PARAMS=${POSITIONAL[@]}
    SRA_IDS=""
    SRA_IDS_COUNTER=0
    while read p; do
        if [ ! -z "$p" ];
        then
            SRA_IDS="${SRA_IDS} ${p}"
            let SRA_IDS_COUNTER=SRA_IDS_COUNTER+1
        fi
    done <${SRA_LIST}
    echo "*** Downloading ${SRA_IDS_COUNTER} SRAs found in ${SRA_LIST} ..."
    cmd=$(printf '%s %s -O %s %s' "$PREFETCH_BIN" "$PREFETCH_PARAMS" "$OUT_DIR" "$SRA_IDS")
    echo "prefetch cmd: ${cmd}" >> "$logfile"
    prefetch_out=()
    while read -r line; do prefetch_out+=("$line"); done < <(
        eval $cmd 2>&1 | tr -d \"\'
    )
    printf "prefetch out: %s\n" "${prefetch_out[@]}" >> "$logfile"
        while read p; do
        if [ ! -z "$p" ];
        then
            printf "processing : %s\n" "${p}" >> "$logfile"
            SRA_IN="${OUT_DIR}/${p}.sra"
            OUT_FILE="${p}.fastq"
            cmd=$(printf '%s -t %s -O %s -o %s %s %s' "$FASTQDUMP_BIN" "$TMP_DIR" "$OUT_DIR" "$OUT_FILE" "${PARAMS[@]}" "${SRA_IN}")
            echo "${DUMP_TOOL} cmd: ${cmd}" >> "$logfile"
            fastqdump_out=()
            while read -r line; do fastqdump_out+=("$line"); done < <(
                eval $cmd 2>&1 | tr -d \"\'
            )
            printf "${DUMP_TOOL} out: %s\n" "${fastqdump_out[@]}" >> "$logfile"
        fi
    done <${SRA_LIST}
    # Delete the SRA file and cache
    rm -f ${OUT_DIR}/*.sra
    rm -rf ${TMP_DIR} > /dev/null 2>&1
    exit 0
fi

if [ -z ${POSITIONAL+x} ];
then
  echo "*** ERROR - Missing SRA accession id. Aborting."
  exit 0
fi

set -- "${POSITIONAL[@]}"
SRA_ID=${POSITIONAL[0]} # PARAMS=${POSITIONAL[@]:0:$((${#POSITIONAL[@]}-1))}
PARAMS=${POSITIONAL[@]:1:$((${#POSITIONAL[@]}))} # SRA_ID=${POSITIONAL[@]:${#POSITIONAL[@]}-1}

if [[ $SRA_ID == SRR* ]] || [[ $SRA_ID == ERR* ]] || [[ $SRA_ID == DRR* ]];
then
  SRA_TYPE=${SRA_ID:0:3}
  SRA_NR=${SRA_ID:3}
  echo "*** Processing ${SRA_TYPE}: $SRA_NR"
else
  echo "*** ERROR - Invalid SRA accession id (allowed: SRR|ERR|DRR). Aborting."
  exit 0
fi

if [ -z ${OUT_FILE+x} ];
then
  OUT_FILE="${SRA_ID}.fastq"
fi

echo "Processing ${SRA_TYPE}: $SRA_NR" > "$logfile"

function validate_ftp(){
  if [[ `wget --max-redirect=0 -S --spider $1  2>&1 | grep -e 'File .* exists\.'` ]];
  then
    echo "Found FTP: ${1}" >> "$logfile"
    return 0
  else
    echo "Missing FTP: ${1}" >> "$logfile"
    return 1
  fi
}

function ftp_download(){
  if validate_ftp $URL;
  then
      echo "*** SRA available for direct FTP download"
      echo "*** Downloading: $URL to $SRA_FILE"
      echo "FTP download: ${URL}" >> "$logfile"
      curl --silent $URL > $TMP_FILE
      # Move to local file only if successful.
      mv $TMP_FILE $SRA_FILE
      SRA_IN=$SRA_FILE
  else
      echo "*** WARNING - SRA not available via FTP [${URL}]"
      SRA_IN=$SRA_ID
  fi
}

function prefetch_download(){
    echo "*** Downloading: PREFETCH $SRA_ID to $SRA_FILE"
    cmd=$(printf '%s %s -o %s %s' "$PREFETCH_BIN" "$PREFETCH_PARAMS" "$SRA_FILE" "$SRA_ID")
    echo "prefetch cmd: ${cmd}" >> "$logfile"   
    prefetch_out=()
    while read -r line; do prefetch_out+=("$line"); done < <(
        eval $cmd 2>&1 | tr -d \"\'
    )
    printf "prefetch out: %s\n" "${prefetch_out[@]}" >> "$logfile"
    
    prefetch_err=1
    for i in "${prefetch_out[@]}"
    do
        if [[ `echo "$i" | grep -e ".*err:.*"` ]]; then
            echo "*** WARNING - prefetch: [ $i ]"
        elif [[ `echo "$i" | grep -e ".*failed to dowload.*"` ]]; then
            prefetch_err=1
            echo "*** WARNING - prefetch: download failed or SRA not available"
        elif [[ `echo "$i" | grep -e ".*failed to download.*"` ]]; then
            prefetch_err=1
            echo "*** WARNING - prefetch: download failed or SRA not available"
        elif [[ `echo "$i" | grep -e ".*was downloaded successfully.*"` ]]; then
            prefetch_err=0
            echo "*** SRA ${SRA_ID} was downloaded successfully";
        fi
    done

    if [[ "$prefetch_err" -eq 1 ]];
    then
        echo "*** WARNING - prefetch: error. falling back to slower ${DUMP_TOOL}"
        SRA_IN=$SRA_ID
        return 0
    else
        SRA_IN=$SRA_FILE
        return 1
    fi
}

if [ -z "$SRA_ID" ]
then
      echo "*** ERROR - Please specify a SRR|ERR|DRR number"
      exit;
fi

# Create the full path to the file.
SRA_FILE="$TMP_DIR/$SRA_ID.sra"
TMP_FILE="$TMP_DIR/$SRA_ID.tmp"

# Download only if it does not exist.
if [ ! -f $SRA_FILE ];
then
    PATH1=${SRA_ID:0:6}
    PATH2=${SRA_ID:0}  
    # PREFETCH download (using ASPERA)
    # URL="anonftp@ftp.ncbi.nlm.nih.gov:/sra/sra-instant/reads/ByRun/sra/${SRA_TYPE}/${PATH1}/${PATH2}/${SRA_ID}.sra"
    prefetch_download
else
    echo "*** SRA file already downloaded: $SRA_FILE"
    SRA_IN=$SRA_FILE
fi

# Run the fasterq-dump.
echo "*** Running ${DUMP_TOOL} on ${SRA_IN}..."
if [ -z ${FASTQ_FALLBACK+x} ]
then
  cmd=$(printf '%s -t %s -O %s -o %s %s %s' "$FASTQDUMP_BIN" "$TMP_DIR" "$OUT_DIR" "$OUT_FILE" "${PARAMS[@]}" "${SRA_IN}")
else
  cmd=$(printf '%s --skip-technical -O %s %s %s' "$FASTQDUMP_BIN" "$OUT_DIR" "${PARAMS[@]}" "${SRA_IN}")
fi
echo "${DUMP_TOOL} cmd: ${cmd}" >> "$logfile"
fastqdump_out=()
while read -r line; do fastqdump_out+=("$line"); done < <(
    eval $cmd 2>&1 | tr -d \"\'
)
printf "${DUMP_TOOL} out: %s\n" "${fastqdump_out[@]}" >> "$logfile"

fastqdump_err=0
for i in "${fastqdump_out[@]}"
do
    if [[ `echo "$i" | grep -e ".*err: invalid accession.*"` ]]; then
        fastqdump_err=2
    elif [[ `echo "$i" | grep -e ".*err:.*"` ]]; then
        fastqdump_err=1
    elif [[ `echo "$i" | grep -e "Read .* spots for .*"` ]]; then
        fastqdump_err=0
    elif [[ `echo "$i" | grep -e "Written .* spots for .*"` ]]; then
        fastqdump_err=0
    fi
done

if [[ "$fastqdump_err" -ge 1 ]];
then
    if [[ "$fastqdump_err" -eq 2 ]];
    then
        echo "*** ERROR - invalid accession id: ${SRA_IN}"
    else
        echo "*** ERROR - ${DUMP_TOOL}: $fastqdump_out"
        URL="ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/${SRA_TYPE}/${PATH1}/${PATH2}/${SRA_ID}.sra"
        if validate_ftp $URL;
        then
            echo "*** WARNING - (Fallback) sratoolkit downloads failed, falling back to ftp"
            # FTP download
            ftp_download

            if [ ! -f $SRA_FILE ];
            then
                echo "*** ERROR - (Fallback) FTP download failed"
            else
                echo "*** Running ${DUMP_TOOL} on ${SRA_IN}..."
                if [ -z ${FASTQ_FALLBACK+x} ]
                then
                  cmd=$(printf '%s -t %s -O %s -o %s %s %s' "$FASTQDUMP_BIN" "$TMP_DIR" "$OUT_DIR" "$OUT_FILE" "${PARAMS[@]}" "${SRA_IN}")
                else
                  cmd=$(printf '%s --skip-technical -O %s %s %s' "$FASTQDUMP_BIN" "$OUT_DIR" "${PARAMS[@]}" "${SRA_IN}")
                fi
                echo "${DUMP_TOOL} fallback cmd: ${cmd}" >> "$logfile"
                fastqdump_out=()
                while read -r line; do fastqdump_out+=("$line"); done < <(
                    eval $cmd 2>&1 | tr -d \"\'
                )
                printf "${DUMP_TOOL} fallback out: %s\n" "${fastqdump_out[@]}" >> "$logfile"

                fastqdump_err=0
                for i in "${fastqdump_out[@]}"
                do
                    if [[ `echo "$i" | grep -e ".*err: invalid accession.*"` ]]; then
                        fastqdump_err=2
                    elif [[ `echo "$i" | grep -e ".*err:.*"` ]]; then
                        fastqdump_err=1
                    fi
                done
                
                if [[ "$fastqdump_err" -ge 1 ]];
                then
                    if [[ "$fastqdump_err" -eq 2 ]];
                    then
                        echo "*** ERROR - (Fallback) invalid accession id: ${SRA_IN}"
                    else
                        echo "*** ERROR - (Fallback) ${DUMP_TOOL}: $fastqdump_out"
                    fi
                    # Delete the SRA file and cache
                    rm -f ${SRA_FILE}*
                    rm -rf ${TMP_DIR} > /dev/null 2>&1
                    exit 1
                fi
            fi
        else
            echo "*** WARNING - (Fallback) no FTP fallback possible for ${SRA_ID} [${URL}]"
        fi
    fi
    # Delete the SRA file and cache
    rm -f ${SRA_FILE}*
    rm -rf ${TMP_DIR} > /dev/null 2>&1
    exit 1
else
    echo "*** Successfully processed: $SRA_IN"
    # Delete the SRA file and cache
    rm -f ${SRA_FILE}*
    rm -rf ${TMP_DIR} > /dev/null 2>&1
fi
